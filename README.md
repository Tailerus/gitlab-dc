Создайте файл /root/docker-compose.yml и добавьте в него следующее:

version: '3.7'
services:
  gitlab:
   container_name: gitlab
   image: 'gitlab/gitlab-ce:latest'
   restart: always
   hostname: '<SERVER_DNS_NAME>'
   environment:
     GITLAB_OMNIBUS_CONFIG: |
       external_url 'https://<SERVER_DNS_NAME>'
       # Add any other gitlab.rb configuration here, each on its own line
   ports:
     - '80:80'
     - '443:443'
     - '22:22'
   volumes:
     - '/opt/gitlab/config:/etc/gitlab'
     - '/opt/gitlab/logs:/var/log/gitlab'
     - '/opt/gitlab/data:/var/opt/gitlab'

  gitlab-runner:
   container_name: gitlab-runner
   image: gitlab/gitlab-runner:latest
   restart: always
   volumes:
     - '/opt/gitlab-runner/data:/home/gitlab_ci_multi_runner/data'
     - '/opt/gitlab-runner/config:/etc/gitlab-runner'
     - '/var/run/docker.sock:/var/run/docker.sock:rw'
   environment:
     - CI_SERVER_URL=https://<SERVER_DNS_NAME>/ci

Этот файл запускает два образа: для gitlab и для gitlab-runner (это pipeline для сборки).

Обратите внимание, что контейнер должен быть доступен из внешней сети по порту 22, иначе при доступе командой git придется указывать нестандартный порт. Поэтому системный ssh перенесите на другой порт. Для этого откройте файл /etc/ssh/sshd_config и найдите строку:

#Port 22 

Если строка закомментирована, раскомментируйте ее и измените номер порта, например:

Port 35242   

Перезапустите sshd:

root@ubuntu-standard-2-4-40gb:/etc/ssh# service sshd restart
Убедитесь, что сервис слушает на новом порту:

root@ubuntu-standard-2-4-40gb:/etc/ssh# netstat -tulpn | grep 35242
tcp 0 0 0.0.0.0:35242 0.0.0.0:\* LISTEN 3625/sshd
tcp6       0      0 :::35242                :::\*                    LISTEN      3625/sshd
Авторизуйтесь по новому порту. Если подключиться не удается, проверьте настройки Firewall.

Создайте необходимые директории для persistent storage gitlab:
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab/config
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab/logs
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab/data
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab-runner
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab-runner/config
root@ubuntu-standard-2-4-40gb:~# mkdir /opt/gitlab-runner/data
Запустите docker-compose:
root@ubuntu-standard-2-4-40gb:~# docker-compose up -d
Creating network "root_default" with the default driver
Pulling gitlab (gitlab/gitlab-ce:latest)...
latest: Pulling from gitlab/gitlab-ce
976a760c94fc: Pull complete
c58992f3c37b: Pull complete
0ca0e5e7f12e: Pull complete
f2a274cc00ca: Pull complete
163f3071a3f8: Pull complete
d96d45e9c9e7: Pull complete
9a0f4e25d3a3: Pull complete
19aad3ea2a1d: Pull complete
fcafd8209320: Pull complete
3a4ea7fd547c: Pull complete
Digest: sha256:f5cb34c4d6bca26734dbce8889863d32c4ce0df02079b8c50bc4ac1dd89b53f4
Status: Downloaded newer image for gitlab/gitlab-ce:latest
Pulling gitlab-runner (gitlab/gitlab-runner:latest)...
latest: Pulling from gitlab/gitlab-runner
7ddbc47eeb70: Pull complete
c1bbdc448b72: Pull complete
8c3b70e39044: Pull complete
45d437916d57: Pull complete
59a312699ead: Pull complete
6562c5999ae2: Pull complete
368e9065e920: Pull complete
b92ce2befcc8: Pull complete
420f91b9ac4d: Pull complete
Digest: sha256:c40748978103959590474b81b72d58f0c240f010b4c229181aaf3132efdf4bd1
Status: Downloaded newer image for gitlab/gitlab-runner:latest
Creating gitlab-runner ... done
Creating gitlab        ... done
Запуск занимает около 5 минут, затем сервис доступен по HTTP. Проверьте состояние запуска:

root@ubuntu-standard-2-4-40gb:~# docker ps
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS                   PORTS                                                          NAMES
bb20bc6cb7d5        gitlab/gitlab-ce:latest       "/assets/wrapper"        10 minutes ago      Up 6 minutes (healthy)   0.0.0.0:22->22/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:443->443/tcp   gitlab
a2209bb357e7        gitlab/gitlab-runner:latest   "/usr/bin/dumb-init ..."   10 minutes ago      Up 10 minutes                                                                           gitlab-runner

Настройка Gitlab

При установке Gitlab генерируются самоподписанные сертификаты для HTTPS. Не будем их использовать, перейдем на сертификаты LetsEncrypt. Для этого откройте файл /opt/gitlab/config/gitlab.rb и приведите к указанному виду следующие параметры:
################################################################################
# Let's Encrypt integration
################################################################################
letsencrypt['enable'] = true
# letsencrypt['contact_emails'] = [] # This should be an array of email addresses to add as contacts
# letsencrypt['group'] = 'root'
# letsencrypt['key_size'] = 2048
# letsencrypt['owner'] = 'root'
# letsencrypt['wwwroot'] = '/var/opt/gitlab/nginx/www'
# See http://docs.gitlab.com/omnibus/settings/ssl.html#automatic-renewal for more on these sesttings
letsencrypt['auto_renew'] = true
letsencrypt['auto_renew_hour'] = 0
letsencrypt['auto_renew_minute'] = 15 # Should be a number or cron expression, if specified.
letsencrypt['auto_renew_day_of_month'] = "\*/7"

В результате будет разрешено использование LetsEncrypt и обновления сертификатов будут проверяться 1 раз в неделю в 00:15.

Перейдите в Docker и запустите перевыпуск сертификатов:
root@ubuntu-standard-2-4-40gb:~# docker exec -it gitlab bash
root@testrom:/# gitlab-ctl reconfigure
Примечание

На момент написания статьи механизм выпуска сертификатов LetsEncrypt работал некорректно из-за изменений в API LetsEncrypt (подробности читайте в кейсах 38255 и 4900). Для решения этой проблемы в файле /opt/gitlab/embedded/cookbooks/letsencrypt/resources/certificate.rb закомментируйте секцию acme_certificate 'staging' do [...] end.
